// Extra types not covered by foundry-pc-types

declare let fromUuid: (uuid: string) => Promise<Entity | null>;

interface Window {
  oneJournal: any;
}

declare class PermissionViewer {
  static directoryRendered: any;
}

declare class JournalDirectory extends SidebarDirectory {}
