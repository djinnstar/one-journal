export const MODULE_NAME = "one-journal"; // TODO: Better handling

export enum settings {
  OPEN_BUTTON_IN_DIRECTORY = "openButtonInSidebarDirectory",
  USE_ONE_JOURNAL = "useOneJournal",
  SIDEBAR_MODE = "sideBarMode",
  GM_ONLY = "gmOnly",
  SYNC_SIDEBAR = "sidebarSync",
  FOLDER_SELECTOR = "folderSelector",
  NO_DUPLICATE_HISTORY = "noDuplicateHistory",
  SIDEBAR_FOLDER_COLOR = "sideBarFolderColor",
  SIDEBAR_DISABLE_PLAYER = "sideBarDisablePlayer",
  SIDEBAR_COLLAPSED = "sideBarCollapsed",
  USE_BROWSER_HISTORY = "useBrowserHistory",
  DBL_CLICK_EDIT = "dblClickEdit",
  SIDEBAR_WIDTH = "sidebarWidth",
  SIDEBAR_COMPACT = "sidebarCompact",
}

interface ICallbacks {
  [setting: string]: (value: any) => void;
}

const noop = () => {};

interface ModuleSetting {
  setting: settings;
  /** Name of the setting that shows up in the list */
  name: string;
  /** Longer description of the setting that shows up in the list */
  hint: string;
  type: any;
  /** Default value */
  default: any;
  /** Creates a multiple choice dropdown with key: value */
  choices?: Object;
  /** Setting should show up in the settings window */
  config?: boolean;
  /** What scope the setting is saved in */
  scope?: "client" | "world";
}

const moduleSettings: ModuleSetting[] = [
  {
    setting: settings.USE_ONE_JOURNAL,
    name: "ONEJOURNAL.SettingsUseOneJournal",
    hint: "ONEJOURNAL.SettingsUseOneJournalHint",
    type: Boolean,
    default: true,
  },
  {
    setting: settings.OPEN_BUTTON_IN_DIRECTORY,
    name: "ONEJOURNAL.SettingsOpenButtonInSidebarDirectory",
    hint: "ONEJOURNAL.SettingsOpenButtonInSidebarDirectoryHint",
    type: Boolean,
    default: false,
  },
  {
    setting: settings.SIDEBAR_MODE,
    name: "ONEJOURNAL.SettingsSidebarMode",
    hint: "ONEJOURNAL.SettingsSidebarModeHint",
    type: String,
    choices: {
      right: "ONEJOURNAL.SettingsSidebarModeRight",
      left: "ONEJOURNAL.SettingsSidebarModeLeft",
    },
    default: "right",
  },
  {
    setting: settings.SIDEBAR_COLLAPSED,
    name: "Sidebar is collapsed",
    hint: "This option should not show up in the settings window",
    type: Boolean,
    default: false,
    config: false, // Doesn't show up in config
  },
  {
    setting: settings.SIDEBAR_FOLDER_COLOR,
    name: "ONEJOURNAL.SettingsSidebarFolderColor",
    hint: "ONEJOURNAL.SettingsSidebarFolderColorHint",
    type: Boolean,
    default: false,
  },
  {
    setting: settings.SYNC_SIDEBAR,
    name: "ONEJOURNAL.SettingsSidebarSync",
    hint: "ONEJOURNAL.SettingsSidebarSyncHint",
    type: Boolean,
    default: true,
  },
  {
    setting: settings.SIDEBAR_WIDTH,
    name: "ONEJOURNAL.SettingsSidebarWidth",
    hint: "ONEJOURNAL.SettingsSidebarWidthHint",
    type: Number,
    choices: {
      150: "ONEJOURNAL.SettingsSidebarWidthTiny",
      230: "ONEJOURNAL.SettingsSidebarWidthSmall",
      275: "ONEJOURNAL.SettingsSidebarWidthReduced",
      300: "ONEJOURNAL.SettingsSidebarWidthNormal",
      336: "ONEJOURNAL.SettingsSidebarWidthWide",
    },
    default: 300,
  },
  {
    setting: settings.SIDEBAR_COMPACT,
    name: "ONEJOURNAL.SettingsSidebarCompact",
    hint: "ONEJOURNAL.SettingsSidebarCompactHint",
    type: Boolean,
    default: false,
  },
  {
    setting: settings.FOLDER_SELECTOR,
    name: "ONEJOURNAL.SettingsFolderSelector",
    hint: "ONEJOURNAL.SettingsFolderSelectorHint",
    type: Boolean,
    default: false,
  },
  {
    setting: settings.DBL_CLICK_EDIT,
    name: "ONEJOURNAL.SettingsDblClickEdit",
    hint: "ONEJOURNAL.SettingsDblClickEditHint",
    type: Boolean,
    default: true,
  },
  {
    setting: settings.NO_DUPLICATE_HISTORY,
    name: "ONEJOURNAL.SettingsNoDuplicateHistory",
    hint: "ONEJOURNAL.SettingsNoDuplicateHistoryHint",
    type: Boolean,
    default: false,
  },
  {
    setting: settings.USE_BROWSER_HISTORY,
    name: "ONEJOURNAL.SettingsUseBrowserHistoryExperimental",
    hint: "ONEJOURNAL.SettingsUseBrowserHistoryHint",
    type: Boolean,
    default: false,
  },
  {
    setting: settings.SIDEBAR_DISABLE_PLAYER,
    name: "ONEJOURNAL.SettingsSidebarDisablePlayer",
    hint: "ONEJOURNAL.SettingsSidebarDisablePlayerHint",
    type: Boolean,
    default: false,
    scope: "world",
  },
  {
    setting: settings.GM_ONLY,
    name: "ONEJOURNAL.SettingsGMOnly",
    hint: "ONEJOURNAL.SettingsGMOnlyHint",
    type: Boolean,
    default: false,
    scope: "world",
  },
];

function registerSetting(callbacks: ICallbacks, { setting, ...options }) {
  game.settings.register(MODULE_NAME, setting, {
    config: true,
    scope: "client",
    ...options,
    onChange: callbacks[setting] || noop,
  });
}

export function registerSettings(callbacks: ICallbacks = {}) {
  moduleSettings.forEach(item => {
    registerSetting(callbacks, item);
  });
}

export function getSetting(setting: settings) {
  return game.settings.get(MODULE_NAME, setting as string);
}

export function setSetting(setting: settings, value: any) {
  return game.settings.set(MODULE_NAME, setting as string, value);
}
